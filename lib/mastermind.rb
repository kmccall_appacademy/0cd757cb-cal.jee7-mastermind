class Code
  attr_reader :pegs

  PEGS = {
    "r" => "Red",
    "g" => "Green",
    "b" => "Blue",
    "y" => "Yellow",
    "o" => "Orange",
    "p" => "Purple"
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(colors)
    colors.downcase.chars.each do |color|
      if !PEGS.keys.include?(color)
        raise "That is not a valid color combination."
      end
    end

    Code.new(colors.downcase.chars)
  end

  def [](idx)
    @pegs[idx]
  end

  def ==(other_code)
    return false if other_code.class != Code
    other_code.pegs == @pegs
  end

  def self.random
    random_colors = []
    num_colors = PEGS.length
    4.times do
      random_colors << PEGS.keys[rand(num_colors)]
    end
    Code.new(random_colors)
  end

  def exact_matches(other_code)
    (0...4).reduce(0) do |matches, idx|
      if self[idx] == other_code[idx]
        matches += 1
      end
      matches
    end
  end

  def near_matches(other_code)
    matching_letters = []
    (0...4).each do |idx|
      if self.pegs.include?(other_code[idx]) && self[idx] != other_code[idx]
        matching_letters << other_code[idx]
      end
    end
    matching_letters.uniq.length
  end

end

class Game
  attr_reader :secret_code
  attr_accessor :last_guess

  def initialize(secret_code=Code.random)
    @secret_code = secret_code
  end

  def get_guess
    guess = "waiting on a guess"
    until guess.chars.length == 4
      puts "Enter a 4 letter color combination."
      guess = $stdin.gets.chomp
    end
    @last_guess = Code.parse(guess)
  end

  def display_matches(code=@last_guess)
    exact_matches = @secret_code.exact_matches(code)
    near_matches = @secret_code.near_matches(code)
    puts "You had #{exact_matches} exact matches."
    puts "You had #{near_matches} near matches."
  end

end
